module.exports = {
  url: process.env.DATABASEURL || "mongodb://localhost/shop",
  port: process.env.PORT || 8081,
  ip: process.env.IP || "localhost",
  esPort: process.env.PORT || 9200
}