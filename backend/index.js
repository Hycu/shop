require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const errorHandler = require("./handlers/error");
const authRoutes = require("./routes/auth");
const commentsRoutes = require("./routes/comments");
const productsRoutes = require("./routes/products");
const config = require("./config");
const seed = require("./seeds");

app.use(cors());
app.use(bodyParser.json());

app.use("/api/auth", authRoutes);
app.use("/api/products/:product_id/comments", commentsRoutes);
app.use("/api/products", productsRoutes);

app.use(function (req, res, next) {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

app.use(errorHandler);

seed.seedDB();

app.listen(config.port, config.ip, function () {
    console.log(`Server started on ${config.ip}:${config.port}`);
})