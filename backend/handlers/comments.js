const db = require("../models/index");

exports.createComment = async function (req, res, next) {
  try {
    const foundUser = await db.User.findById(res.locals.userId);
    const comment = await db.Comment.create({
      text: req.body.text,
      author: {
        username: foundUser.username,
        id: foundUser._id
      }
    });
    const foundProduct = await db.Product.findById(req.params.product_id);
    foundProduct.comments.push(comment._id);
    foundProduct.save();
    const foundComment = await db.Comment.findById(comment._id).populate("user", {
      username: true,
      profileImageUrl: true
    });
    return res.status(200).json(foundComment);
  } catch (err) {
    return next(err);
  }
};

exports.getComment = async function (req, res, next) {
  try {
    const comment = await db.Comment.findById(req.params.comment_id);
    return res.status(200).json(comment);
  } catch (err) {
    return next(err);
  }
};

exports.deleteComment = async function (req, res, next) {
  try {
    const foundComment = await db.Comment.findByIdAndRemove(req.params.comment_id);
    return res.status(200).json(foundComment);
  } catch (err) {
    return next(err);
  }
};

exports.editComment = async function (req, res, next) {
  try {
    await db.Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment);
    const newComment = await db.Comment.findById(req.params.comment_id);
    return res.status(200).json(newComment);
  } catch (err) {
    return next(err);
  }
};

exports.getComments = async function (req, res, next) {
  try {
    const foundProduct = await db.Product.findById(req.params.product_id);
    let comments = [];
    for (let comment_id of foundProduct.comments) {
      const comment = await db.Comment.findById(comment_id);
      comments.push(comment);
    }
    comments.sort({ createdAt: "desc" });
    return res.status(200).json(comments);
  } catch (err) {
    return next(err);
  }
};