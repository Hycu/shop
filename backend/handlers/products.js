const db = require("../models/index");

exports.createProduct = async function (req, res, next) {
  try {
    const foundUser = await db.User.findById(res.locals.userId);
    const product = await db.Product.create({
      name: req.body.name,
      price: req.body.price,
      image: req.body.image,
      description: req.body.description,
      infos: req.body.infos,
      category: req.body.category,
      author: {
        username: foundUser.username,
        id: foundUser._id
      }
    });
    const foundProduct = await db.Product.findById(product._id).populate("user", {
      username: true,
      profileImageUrl: true
    });
    return res.status(200).json(foundProduct);
  } catch (err) {
    return next(err);
  }
};

exports.getProduct = async function (req, res, next) {
  try {
    let product = await db.Product.findById(req.params.product_id);
    return res.status(200).json([product]);
  } catch (err) {
    return next(err);
  }
};

exports.deleteProduct = async function (req, res, next) {
  try {
    const foundProduct = await db.Product.findById(req.params.product_id);
    for (let comment of foundProduct.comments) {
      await db.Comment.findByIdAndRemove(comment._id);
    }
    const deletedProduct = await foundProduct.remove();
    return res.status(200).json(deletedProduct);
  } catch (err) {
    return next(err);
  }
};

exports.getProducts = async function (req, res, next) {
  try {
    let filter = req.query.category ? { category: req.query.category } : {};
    let idsEs = false;
    if (req.query.search && req.query.search.length > 0) {
      idsEs = await this.getIdsFromEs(req.query.search);
      filter._id = idsEs;
    }
    const pageSize = req.query.pageSize ? req.query.pageSize : 0;
    const currentPage = req.query.currentPage ? req.query.currentPage : 1;
    const sortType = req.query.sort ? { price: req.query.sort } : { price: "asc" };
    let products = await db.Product.find(filter)
      .sort(sortType)
      .skip(pageSize * (currentPage - 1))
      .limit(Number(pageSize));
    return res.status(200).json(products);
  } catch (err) {
    return next(err);
  }
};

getIdsFromEs = async function (search) {
  const products = await db.esClient.search({
    index: "products",
    q: `name:*${search}*`,
    size: 10000,
    default_operator: "AND"
  });
  let productIds = [];
  for (let product of products.hits.hits) {
    productIds.push(product._id);
  }
  console.log(search);
  return productIds;
}

exports.editProduct = async function (req, res, next) {
  try {
    await db.Product.findByIdAndUpdate(req.params.product_id, req.body);
    const newProduct = await db.Product.findById(req.params.product_id);
    return res.status(200).json(newProduct);
  } catch (err) {
    return next(err);
  }
}