require("dotenv").load();
const jwt = require("jsonwebtoken");
const db = require("../models/index");

exports.loginRequired = function (req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, process.env.SECRET_KEY, function (err, decoded) {
      if (decoded) {
        res.locals.userId = decoded.id;
        return next();
      } else {
        return next({
          status: 401,
          message: "Please log in first"
        });
      }
    });
  } catch (err) {
    return next({
      status: 401,
      message: "Please log in first"
    });
  }
};

exports.ensureCorrectUser = async function (req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    let foundItem;
    if (req.params.product_id) {
      foundItem = await db.Product.findById(req.params.product_id);
    } else if (req.params.comment_id) {
      foundItem = await db.Comment.findById(req.params.comment_id);
    }
    jwt.verify(token, process.env.SECRET_KEY, function (err, decoded) {
      if (decoded && decoded.id === String(foundItem.author.id)) {
        return next();
      } else {
        return next({
          status: 401,
          message: "Unauthorized"
        });
      }
    });
  } catch (err) {
    return next({
      status: 401,
      message: "Unauthorized"
    });
  }
}

exports.tokenValidation = async function (req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, process.env.SECRET_KEY, async function (err, decoded) {
      if (decoded) {
        const user = await db.User.findById(decoded.id);
        console.log(user)
        if (user && user.username === decoded.username) {
          return next();
        } else {
          return next({
            status: 401,
            message: "Session expired"
          });
        }
      }
    });
  } catch (err) {
    return next({
      status: 401,
      message: "Session expired"
    });
  }
}