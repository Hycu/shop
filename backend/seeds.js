const mongoose = require("mongoose");
const db = require("./models/index");
const faker = require("faker");
const axios = require("axios");
const config = require("./config");

module.exports = {
  seedDB: async function () {
    axios["delete"](`http://${config.ip}:${config.esPort}/products`);
    await mongoose.connection.collections["users"].drop();
    await mongoose.connection.collections["comments"].drop();
    await mongoose.connection.collections["products"].drop();
    const user = await db.User.create({
      username: faker.internet.userName(),
      password: "testPass",
      email: faker.internet.email()
    });
    let cat = "";
    let img = "https://images.unsplash.com/photo-1484704849700-f032a568e944?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=04c4af64629678d376f21fd9bd05229f&auto=format&fit=crop&w=1050&q=80";
    for (let j = 0; j < 3; j++) {
      switch (cat) {
        case "":
          cat = "Casual";
          break;
        case "Casual":
          cat = "Office";
          img = "https://images.unsplash.com/3/www.madebyvadim.com.jpg?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0d00b2e84a1695cf3c46c8eda5548195&auto=format&fit=crop&w=1060&q=80";
          break;
        case "Office":
          cat = "Outdoor";
          img = "https://images.unsplash.com/photo-1485965120184-e220f721d03e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=51a9aa4dd828bf5d50fcd8154dc405b8&auto=format&fit=crop&w=1050&q=80";
          break;
      }
      for (let i = 0; i < 20; i++) {
        const seed = {
          name: faker.commerce.productName(),
          price: faker.commerce.price(),
          image: img,
          description: faker.lorem.paragraph(),
          author: {
            id: user._id,
            username: user.username
          },
          infos: [
            faker.commerce.productMaterial(),
            faker.commerce.productAdjective(),
            faker.commerce.color()
          ],
          category: cat
        }
        let product = await db.Product.create(seed);
        const comment = await db.Comment.create({
          text: faker.lorem.paragraph(),
          createdAt: faker.date.past(),
          author: {
            id: user._id,
            username: user.username
          }
        });
        product.comments.push(comment._id);
        product.save();
      }
    }
  }
};