const express = require("express");
const router = express.Router({ mergeParams: true });
const { createComment, getComment, deleteComment, editComment, getComments } = require("../handlers/comments");
const { loginRequired, tokenValidation, ensureCorrectUser } = require("../middleware/auth");

router
  .route("/")
  .post(loginRequired, tokenValidation, createComment)
  .get(getComments);
router
  .route("/:comment_id")
  .get(getComment)
  .delete(loginRequired, tokenValidation, ensureCorrectUser, deleteComment)
  .put(loginRequired, tokenValidation, ensureCorrectUser, editComment);

module.exports = router;