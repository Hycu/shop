const express = require("express");
const router = express.Router({ mergeParams: true });
const { loginRequired, ensureCorrectUser, tokenValidation } = require("../middleware/auth");
const { getProducts, createProduct, getProduct, deleteProduct, editProduct } = require("../handlers/products");

router
  .route("/")
  .post(loginRequired, tokenValidation, createProduct)
  .get(getProducts);
router
  .route("/:product_id")
  .get(getProduct)
  .delete(loginRequired, tokenValidation, ensureCorrectUser, deleteProduct)
  .put(loginRequired, tokenValidation, ensureCorrectUser, editProduct);

module.exports = router;