const mongoose = require("mongoose");
const mongoosastic = require("mongoosastic");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        es_indexed: true
    },
    price: {
        type: Number,
        required: true
    },
    image: String,
    description: {
        type: String,
        required: true
    },
    createdAt: { type: Date, default: Date.now },
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
        },
        username: {
            type: String,
            required: true
        }
    },
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comment"
        }],
    infos: [String],
    category: {
        type: String,
        required: true
    }
},
    {
        timestamps: true
    });

productSchema.plugin(mongoosastic);

const Product = mongoose.model("Product", productSchema);

module.exports = Product;