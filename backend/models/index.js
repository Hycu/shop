const mongoose = require("mongoose");
const config = require("../config");
const elasticsearch = require("elasticsearch");

const esClient = new elasticsearch.Client({
    host: `${config.ip}:${config.esPort}`
});

mongoose.set("debug", true);
mongoose.Promise = Promise;
mongoose.connect(config.url, {
  keepAlive: true
});

module.exports.User = require("./user");
module.exports.Comment = require("./comment");
module.exports.Product = require("./product");
module.exports.esClient = esClient;