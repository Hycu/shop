const mongoose = require("mongoose");
const User = require("./user");

const commentSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  author: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    username: String
  }
},
  {
    timestamps: true
  });

commentSchema.pre("remove", async function (next) {
  try {
    const user = await User.findById(this.user);
    user.Comments.remove(this.id);
    await user.save();
    return next();
  } catch (err) {
    return next(err);
  }
});

const Comment = mongoose.model("Comment", commentSchema);

module.exports = Comment;