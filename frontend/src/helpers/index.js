export function addQuery(oldQuery, query){
  let newQuery = oldQuery;
  if (query === "") {
    newQuery = query;
  } else if (oldQuery === "") {
    newQuery = "?" + query + "&";
  } else if (!oldQuery.includes(query.substr(0, query.indexOf("=")))) {
    newQuery = oldQuery + query + "&";
  } else if (oldQuery.includes(query.substr(0, query.indexOf("=")))) {
    const first = query.substr(0, query.indexOf("="));
    const start = Number(oldQuery.indexOf(first));
    const end = Number(oldQuery.indexOf("&", start));
    newQuery = oldQuery.replace(oldQuery.substr(start, end-start), query);
    newQuery = newQuery[newQuery.length - 1] !== "&" ? newQuery + "&" : newQuery;
  }
  return newQuery;
}