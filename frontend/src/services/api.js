import axios from "axios";
import { logout } from "../store/actions/auth";


export function setTokenHeader(token) {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
}

export function apiCall(method, path, data, apiPrefix = "http://localhost:8081") {
  return new Promise((resolve, reject) => {
    return axios[method](apiPrefix + path, data).then(res => {
      return resolve(res.data);
    }).catch(err => {
      return reject(err.response.data.error);
    });
  });
}