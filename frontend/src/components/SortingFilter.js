import React from "react";
import { Link } from "react-router-dom";
import { addQuery } from "../helpers";
import { Menu, Icon, Popup } from 'semantic-ui-react'

const SortingFilter = ({ query, setQuery }) => {

  return (
    <div>
      <Menu.Header as="h3">Price sort:</Menu.Header>
      <Menu widths={2} fluid secondary>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(query, `sort=1`) }} onClick={() => setQuery(addQuery(query, `sort=1`))}><Popup trigger={<Icon color="teal" size="large" name="sort numeric up" />} content="Sort ascending" /></Link></Menu.Item>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(query, `sort=-1`) }} onClick={() => setQuery(addQuery(query, `sort=-1`))}><Popup trigger={<Icon color="teal" size="large" name="sort numeric down" />} content="Sort descending" /></Link></Menu.Item>
      </Menu>
    </div>
  )
};

export default SortingFilter;