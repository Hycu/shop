import React, { Component } from "react";
import { Switch, Route, withRouter, Redirect, Link } from "react-router-dom";
import { Button, Header, Message, Grid, Divider, Container } from 'semantic-ui-react';

class Homepage extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.hideNavbar();
  }
  componentWillUnmount() {
    this.props.showNavbar();
  }

  render() {
    return (
      <Grid centered columns="equal" >
        <Container>
          <Divider hidden />
          {this.props.error && (
            <Message negative>
              {this.props.error}
            </Message>
          )}
          <div id="home-header">
            <Header as="h1">Welcome to the Shop!</Header>
            <Link to="/products">
              <Button color="teal">Show available products</Button>
            </Link>
          </div>
          <ul className="slideshow">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </Container>
      </Grid>
    );
  }
};

export default Homepage;