import React from "react";
import { Link } from "react-router-dom";
import { addQuery } from "../helpers";
import { Button } from 'semantic-ui-react'

const PageChanger = ({ query, setQuery, products, recordNum }) => {
  recordNum = isNaN(recordNum) || recordNum === 0 ? Number(products.length) === 0 ? 1 : products.length : recordNum;
  let currentPageStart = Number(query.indexOf("currentPage="));
  currentPageStart = currentPageStart > 0 ? currentPageStart + 12 : currentPageStart;
  const currentPageEnd = Number(query.indexOf("&", currentPageStart));
  const currentPage = (currentPageStart < 0 || currentPageEnd < 0) ? 1 : Number(query.substr(currentPageStart, currentPageEnd - currentPageStart));
  let pageSizeStart = Number(query.indexOf("pageSize="));
  pageSizeStart = pageSizeStart > 0 ? pageSizeStart + 9 : pageSizeStart;
  const pageSizeEnd = Number(query.indexOf("&", pageSizeStart));
  const pageSize = (pageSizeStart < 0 || pageSizeEnd < 0) ? 0 : Number(query.substr(pageSizeStart, pageSizeEnd - pageSizeStart));
  const prevStyle = Number(currentPage) === 1 ? { disabled: true } : { disabled: false, backgroundColor: "teal", color: "white" };
  const tempPageSize = pageSize === 0 ? recordNum : pageSize;
  const maxPages = isNaN(Math.ceil(recordNum / tempPageSize)) ? 1 : Math.ceil(recordNum / tempPageSize);
  const nextStyle = (pageSize === 0 || Number(currentPage) === maxPages) ? { disabled: true } : { disabled: false, backgroundColor: "teal", color: "white" };
  const prevLinkStyle = prevStyle.disabled ? { color: "gray" } : { color: "white" };
  const nextLinkStyle = nextStyle.disabled ? { color: "gray" } : { color: "white" };
  return (
    <Button.Group>
      <Button disabled={prevStyle.disabled} style={prevStyle}><Link className="link" style={prevLinkStyle} to={{ pathname: "/products", search: addQuery(query, `currentPage=${currentPage - 1}`) }} onClick={() => setQuery(addQuery(query, `currentPage=${currentPage - 1}`))}>Previous page</Link></Button>
      <Link to={query}><Button.Or text={`${currentPage}/${maxPages}`} /></Link>
      <Button disabled={nextStyle.disabled} style={nextStyle}><Link className="link" style={nextLinkStyle} to={{ pathname: "/products", search: addQuery(query, `currentPage=${currentPage + 1}`) }} onClick={() => setQuery(addQuery(query, `currentPage=${currentPage + 1}`))}>Next page</Link></Button>
    </Button.Group>
  )
};
export default PageChanger;