import React, { Component } from "react";
import { Message, Grid, Header, Segment, Divider, Form, Input, Button } from 'semantic-ui-react';
import { connect } from "react-redux";

class AuthForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
      // profileImageUrl: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    if(this.props.currentUser.isAuthenticated){
      this.props.history.push("/products");
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const authType = this.props.signUp ? "signup" : "signin";
    this.props.onAuth(authType, this.state).then(() => {
      this.props.history.push("/");
    }).catch(() => {
      return;
    })
  }

  render() {
    const { email, username, password/*, profileImageUrl*/ } = this.state;
    const {
      heading,
      buttonText,
      signUp,
      error,
      removeError,
      history
    } = this.props;

    history.listen(() => {
      removeError();
    });

    return (
      <Grid centered columns="equal">
        <Divider hidden />
        <Grid.Row>
          {error.message && (
            <Message negative>
              {error.message}
            </Message>
          )}
        </Grid.Row>
        <Segment raised color="teal">
          <Form onSubmit={this.handleSubmit}>
            <Header as="h2">{heading}</Header>
            {signUp && (
              <div>
                <Grid.Row>
                  <Input
                    transparent
                    id="username"
                    name="username"
                    placeholder="Username..."
                    onChange={this.handleChange}
                    value={username}
                    type="text"
                  />
                  {/* <label htmlFor="image-url">Image URL:</label>
                  <input
                    className="form-control"
                    id="image-url"
                    name="profileImageUrl"
                    onChange={this.handleChange}
                    value={profileImageUrl}
                    type="text"
                  /> */}
                </Grid.Row>
                <Divider />
              </div>
            )}
            <Grid.Row>
              <Input
                transparent
                placeholder="Email..."
                id="email"
                name="email"
                onChange={this.handleChange}
                value={email}
                type="text"
              />
            </Grid.Row>
            <Divider />
            <Grid.Row>
              <Input
                transparent
                placeholder="Password..."
                id="password"
                name="password"
                onChange={this.handleChange}
                type="password"
              />
            </Grid.Row>
            <Divider />
            <Button color="teal" type="submit">
              {buttonText}
            </Button>
          </Form>
        </Segment>
      </Grid>
    );
  }
}


function mapStateToProps(state) {
  return {
    error: state.error,
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps)(AuthForm);