import React from "react";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { Image, List, Segment, Header, Icon, Popup, Container } from 'semantic-ui-react'

const ProductPreview = ({
  name,
  price,
  image,
  text,
  username,
  removeProduct,
  isCorrectUser,
  id,
  category
}) => (
    <Segment color="teal" raised>
      <Container>
        <List.Item fluid="true">
          <Link to={`/products/${id}`} floated="left">
            <Image
              src={image}
              alt={name}
              height="150"
              width="150"
              className="timeline-image" floated="left" />
          </Link>
          <List.Content floated="right">
            <List.Header as="h3" floated="left">
              <Link to={`/products/${id}`}>
                {name}
              </Link>
              <Header floated="right">
                ${price}
              </Header>
            </List.Header>
            <Header as="h4">{category}
              <Header floated="right">
                {(isCorrectUser && (
                  <a onClick={removeProduct}><Popup trigger={<Icon color="red" name="delete" size="large" />} content="Delete product" basic />
                  </a>))}
              </Header>
            </Header>
            <List.Description>{text}</List.Description>
          </List.Content>
        </List.Item>
      </Container>
    </Segment>
  );

export default ProductPreview;