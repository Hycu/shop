import React from "react";
import { Link } from "react-router-dom";
import { addQuery } from "../helpers";
import { Menu, Popup, Icon, Container } from 'semantic-ui-react'

const CategoryFilter = ({ query, setQuery }) => {

  return (
    // <div className="panel panel-default">
    <div>
      <Menu.Header as="h3">Category filter:</Menu.Header>
      <Menu fluid secondary>
        <Menu.Item>
          <Link to={
            { pathname: "/products", search: addQuery(addQuery(query, `category=Office`), `currentPage=1`) }
          }
            onClick={() => setQuery(addQuery(addQuery(query, `category=Office`), `currentPage=1`))}>
            <Popup trigger={<Icon name="briefcase" size="large" color="teal" />} content="Office" />
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to={
            { pathname: "/products", search: addQuery(addQuery(query, `category=Casual`), `currentPage=1`) }
          }
            onClick={() => setQuery(addQuery(addQuery(query, `category=Casual`), `currentPage=1`))}>
            <Popup trigger={<Icon name="beer" size="large" color="teal" />} content="Casual" />
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to={
            { pathname: "/products", search: addQuery(addQuery(query, `category=Outdoor`), `currentPage=1`) }
          }
            onClick={() => setQuery(addQuery(addQuery(query, `category=Outdoor`), `currentPage=1`))}>
            <Popup trigger={<Icon name="tree" size="large" color="teal" />} content="Outdoor" />
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to={
            { pathname: "/products", search: addQuery(addQuery(query, `category=`), `currentPage=1`) }
          }
            onClick={() => setQuery(addQuery(addQuery(query, `category=`), `currentPage=1`))}>
            <Popup trigger={<Icon name="globe" size="large" color="teal" />} content="All categories" />
          </Link>
        </Menu.Item>
      </Menu>
    </div>
    // {/* </div> */}
  )
};

export default CategoryFilter;