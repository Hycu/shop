import React from "react";
import { Link } from "react-router-dom";
import { addQuery } from "../helpers";
import { Menu } from 'semantic-ui-react'

const pageSizer = ({ query, setQuery }) => {

  return (
    <div>
      <Menu.Header as="h3">Page size:</Menu.Header>
      <Menu fluid secondary>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(addQuery(query, `pageSize=10`), `currentPage=1`) }} onClick={() => setQuery(addQuery(addQuery(query, `pageSize=10`), `currentPage=1`))}>10</Link></Menu.Item>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(addQuery(query, `pageSize=20`), `currentPage=1`) }} onClick={() => setQuery(addQuery(addQuery(query, `pageSize=20`), `currentPage=1`))}>20</Link></Menu.Item>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(addQuery(query, `pageSize=50`), `currentPage=1`) }} onClick={() => setQuery(addQuery(addQuery(query, `pageSize=50`), `currentPage=1`))} >50</Link></Menu.Item>
      <Menu.Item><Link to={{ pathname: "/products", search: addQuery(addQuery(query, `pageSize=0`), `currentPage=1`) }} onClick={() => setQuery(addQuery(addQuery(query, `pageSize=0`), `currentPage=1`))}>Show all</Link></Menu.Item>
      </Menu>
    </div>
  )
};

export default pageSizer;