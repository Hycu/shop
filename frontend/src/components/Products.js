import React, { Component } from "react";
import ProductList from "../containers/ProductList";
import { fetchProducts, removeProduct } from "../store/actions/product";
import CategoryFilter from "./CategoryFilter";
import SortingFilter from "./SortingFilter";
import PageChanger from "./PageChanger";
import PageSizer from "./PageSizer";
import { connect } from "react-redux";
import { Input } from 'semantic-ui-react'
import { addQuery } from "../helpers";
import { Message, Grid, Container, Segment, Divider, Loader } from 'semantic-ui-react';


class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      maxRecords: 0,
      casualRecords: 0,
      officeRecords: 0,
      outdoorRecords: 0,
      foundRecords: 0,
      search: "",
      loading: true
    };
    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async calculateRecordsAndUpdateState() {
    const maxRecords = this.props.products.length;
    let casualRecords = 0;
    let officeRecords = 0;
    let outdoorRecords = 0;
    for (let record of this.props.products) {
      if (record.category === "Casual") {
        casualRecords++;
      } else if (record.category === "Office") {
        officeRecords++;
      } else if (record.category === "Outdoor") {
        outdoorRecords++;
      }
    }
    await this.setState({ maxRecords, casualRecords, officeRecords, outdoorRecords, loading: false });
  }

  async componentWillMount() {
    await this.props.fetchProducts(this.state.query);
    await this.calculateRecordsAndUpdateState();
  }

  async handleQueryChange(newQuery) {
    let foundRecords = this.state.foundRecords;
    if (newQuery.includes("search=") && !newQuery.includes("search=&") && (!newQuery.includes("currentPage=") || newQuery.includes("currentPage=&") || newQuery.includes("currentPage=1"))) {
      await this.props.fetchProducts(addQuery(newQuery, "pageSize=0"));
      foundRecords = this.props.products.length;
    }
    await this.props.fetchProducts(newQuery);
    await this.setState({ query: newQuery, foundRecords });
  }

  async handleRemove([id, category]) {
    await this.props.removeProduct(id);
    const maxRecords = this.state.maxRecords - 1;
    let newState = { maxRecords };
    if (category === "Casual") {
      newState.casualRecords = this.state.casualRecords - 1;
    } else if (category === "Office") {
      newState.officeRecords = this.state.officeRecords - 1;
    } else if (category === "Outdoor") {
      newState.outdoorRecords = this.state.outdoorRecords - 1;
    }
    await this.setState(newState);
  }

  async handleChange(event) {
    await this.setState({ [event.target.name]: event.target.value });
    const newQuery = addQuery(addQuery(this.state.query, `search=${this.state.search}`), "currentPage=1");
    await this.props.fetchProducts(addQuery(addQuery(addQuery(this.state.query, `search=${this.state.search}`), "currentPage=1"), "pageSize=0"));
    await this.setState({ query: newQuery, foundRecords: this.props.products.length });
    await this.props.fetchProducts(newQuery);
    // this.props.history.push(this.state.query);
  }

  render() {
    const { history, removeError, products } = this.props;
    history.listen(() => {
      removeError();
    });
    let tempMaxPages = 0;
    if ((this.state.query.includes("category=&") || !this.state.query.includes("category=") && (this.state.query.includes("search=&") || !this.state.query.includes("search=")))) {
      tempMaxPages = this.state.maxRecords;
    } else if (!this.state.query.includes("category=&") && this.state.query.includes("category=")) {
      const categoryStart = Number(this.state.query.indexOf("category=")) + 9;
      const categoryEnd = Number(this.state.query.indexOf("&", categoryStart));
      let category = this.state.query;
      category = category.substr(categoryStart, categoryEnd - categoryStart);
      if (category === "Casual") {
        tempMaxPages = this.state.casualRecords;
      } else if (category === "Office") {
        tempMaxPages = this.state.officeRecords;
      } else if (category === "Outdoor") {
        tempMaxPages = this.state.outdoorRecords;
      }
    }
    if (!this.state.query.includes("search=&") && this.state.query.includes("search=")) {
      tempMaxPages = this.state.foundRecords;
    }

    if (this.state.loading) {
      return (
        <Loader active />
      );
    } else {
      return (
        <Grid centered columns="equal">
          <Grid.Row>
            {this.props.error.message && (
              <Message negative>
                {this.props.error.message}
              </Message>
            )}
          </Grid.Row>
          <Grid.Row>
            <Segment raised color="teal">
              <Input
                transparent
                placeholder="Search..."
                value={this.state.search}
                onChange={this.handleChange}
                type="text"
                name="search"
              />
            </Segment>
          </Grid.Row>
          <Grid.Row>
            <PageChanger {...this.props} recordNum={tempMaxPages} query={this.state.query} setQuery={this.handleQueryChange} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={4} floated="left">
              <Segment color="teal" fluid="true" floated="left" raised>
                <CategoryFilter {...this.props} query={this.state.query} setQuery={this.handleQueryChange} />
                <Divider />
                <SortingFilter {...this.props} query={this.state.query} setQuery={this.handleQueryChange} />
                <Divider />
                <PageSizer {...this.props} query={this.state.query} setQuery={this.handleQueryChange} />
              </Segment>
            </Grid.Column>
            <Grid.Column floated="right" width={12}>
              {products && products.length > 0 && (
                <Container floated="right">
                  <ProductList {...this.props} handleRemove={this.handleRemove} />
                </Container>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      );
    }
  }
};

function mapStateToProps(state) {
  return {
    products: state.products,
    currentUser: state.currentUser,
    query: state.query
  };
}

export default connect(mapStateToProps, { fetchProducts, removeProduct, addQuery })(Products);