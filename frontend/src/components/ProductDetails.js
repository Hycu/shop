import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchProducts, removeProduct } from "../store/actions/product";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { Popup, Segment, Grid, List, Message, Icon, Header, Container, Image, Divider, Menu, Loader } from 'semantic-ui-react'

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",
      loading: true
    }
    this.handleRemoveProduct = this.handleRemoveProduct.bind(this);
  }

  async componentWillMount() {
    await this.props.fetchProducts(`/${this.props.match.params.product_id}`);
    await this.setState({ loading: false });
  }

  async handleRemoveProduct() {
    await this.props.removeProduct(this.props.match.params.product_id);
    this.props.history.push("/");
  }

  render() {
    const { products, history, removeError, error, currentUser, removeProduct } = this.props;

    history.listen(() => {
      removeError();
    });
    Moment.globalFormat = 'DD.MM.YYYY HH:MM';
    let loaded = false;
    let hasComments = false;
    let isAuthor = false;
    let comments = [];
    if (products && products.length > 0) {
      loaded = true;
      if (currentUser.isAuthenticated && currentUser.user.id === products[0].author.id) {
        isAuthor = true;
      }
      if (products[0].comments && products[0].comments.length > 0) {
        hasComments = true;
        for (let comment of products[0].comments) {
          comments.push(
            <div className="single-comment">
              {comment.text}
            </div>);
        }
      }
    }
    if (this.state.loading) {
      return (
        <Loader active />
      );
    } else {
      return (
        <Grid centered columns="equal">
          <Grid.Row>
            {error.message && (
              <Message negative>
                {error.message}
              </Message>
            )}
          </Grid.Row>
          {loaded && (
            <Grid.Row>
              <Grid.Column width={4} floated="left">
                <Grid.Row>
                  <Segment raised floated="left" color="teal">
                    <List celled fluid="true">
                      <List.Header as="h3">{products[0].category}</List.Header>
                      <List.Item><List.Header as="h4">Material:</List.Header>{products[0].infos[0].toUpperCase()}</List.Item>
                      <List.Item><List.Header as="h4">Adjective:</List.Header>{products[0].infos[1].toUpperCase()}</List.Item>
                      <List.Item><List.Header as="h4">Color:</List.Header>{products[0].infos[2].toUpperCase()}</List.Item>
                    </List>
                  </Segment>
                </Grid.Row>
              </Grid.Column>
              <Grid.Column width={10}>
                <Segment raised color="teal">
                  <Container>
                    <Grid.Row>
                      <Header as="h1">{products[0].name}<Header floated="right">${products[0].price}</Header></Header>
                      <Divider />
                    </Grid.Row>
                    <Grid.Row><Image className="detailed-image" src={products[0].image} /></Grid.Row>
                    <Divider />
                    <Grid.Row>{products[0].description}</Grid.Row>
                    <Divider />
                    <Grid.Row>
                      <Menu secondary>
                        <Menu.Item>Created:<Menu.Item style={{ fontWeight: "bold" }}><Moment>{products[0].createdAt}</Moment></Menu.Item></Menu.Item>
                        <Menu.Item position="right">Author:<Menu.Item style={{ fontWeight: "bold" }}>{products[0].author.username}</Menu.Item></Menu.Item>
                      </Menu>
                    </Grid.Row>
                    <div className="comment-container">
                      <div className="comment-input">

                      </div>
                      <div className="comment-output">
                        {hasComments && (
                          comments
                        )}
                      </div>
                    </div>
                  </Container>
                </Segment>
              </Grid.Column>
              {isAuthor && (
                <Grid.Column width={2} floated="right">
                  <Segment raised floated="left" color="teal">
                    <Link to={{ pathname: `/products/${this.props.match.params.product_id}/edit` }}><Popup trigger={<Icon name="edit outline" size="large" color="yellow" />} content="Edit product" /></Link>
                    <button onClick={() => this.handleRemoveProduct()}><Popup trigger={<Icon name="delete" size="large" color="red" />} content="Delete product" /></button>
                  </Segment>
                </Grid.Column>
              )}
            </Grid.Row>
          )}
        </Grid>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    products: state.products,
    currentUser: state.currentUser,
    query: state.query
  };
}

export default connect(mapStateToProps, { fetchProducts, removeProduct })(ProductDetails);