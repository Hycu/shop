import { apiCall } from "../../services/api";
import { addError } from "./error";
import { LOAD_PRODUCTS, REMOVE_PRODUCT } from "../actionTypes";

export const loadProducts = products => ({
  type: LOAD_PRODUCTS,
  products
});

export const remove = id => ({
  type: REMOVE_PRODUCT,
  id
});

export const removeProduct = (product_id) => {
  return (dispatch, getState) => {
    const { currentUser } = getState();
    return apiCall("delete", `/api/products/${product_id}`, {
      author: {
        id: currentUser.user.id,
        username: currentUser.user.username
      }
    })
      .then(() => dispatch(remove(product_id)))
      .catch(err => dispatch(addError(err.message)));
  }
}

export const fetchProducts = (query = "/") => {
  return dispatch => {
    return apiCall("get", "/api/products" + query)
      .then(res => {
        dispatch(loadProducts(res));
      })
      .catch(err => {
        dispatch(addError(err.message));
      });
  };
};

export const postNewProduct = productDetails => (dispatch, getState) => {
  const { currentUser } = getState();
  return apiCall("post", `/api/products`, {
    ...productDetails, author: {
      id: currentUser.user.id,
      username: currentUser.user.username
    }
  })
    .then(res => { })
    .catch(err => dispatch(addError(err.message)));
};

export const editProduct = (productDetails, id) => (dispatch) => {
  return apiCall("put", `/api/products/${id}`, productDetails)
    .then(res => { })
    .catch(err => dispatch(addError(err.message)));
};