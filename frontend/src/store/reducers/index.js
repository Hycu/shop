import { combineReducers } from "redux";
import currentUser from "./currentUser";
import error from "./error";
import products from "./products";

const rootReducer = combineReducers({
  currentUser,
  error,
  products
});

export default rootReducer;