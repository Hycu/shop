import React from "react";
import { Switch, Route, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Homepage from "../components/Homepage";
import AuthForm from "../components/AuthForm";
import { authUser, logout } from "../store/actions/auth";
import { removeError } from "../store/actions/error";
import withAuth from "../hocs/withAuth";
import ProductForm from "../containers/ProductForm";
import Products from "../components/Products";
import ProductDetails from "../components/ProductDetails";
import { fetchProducts, removeProduct } from "../store/actions/product";
import { Container } from 'semantic-ui-react';

const Main = props => {
  const { authUser, error, removeError, currentUser, logout, hideNavbar, showNavbar } = props;
  if (error && error.message === "Session expired") {
    logout();
  }
  return (
    <Container>
      <Switch>
        <Route exact path="/" render={props =>
          <Homepage hideNavbar={hideNavbar} showNavbar={showNavbar} currentUser={currentUser} {...props} />
        } />
        <Route exact path="/signin" render={props =>
          <AuthForm
            removeError={removeError}
            error={error}
            onAuth={authUser}
            buttonText="Log in"
            heading="Welcome back"
            {...props} />
        } />
        <Route exact path="/signup" render={props =>
          <AuthForm
            removeError={removeError}
            error={error}
            onAuth={authUser}
            signUp
            buttonText="Sign me up"
            heading="Join our shop today"
            {...props} />
        } />
        <Route exact path="/products/new" component={withAuth(ProductForm)} />
        <Route exact path="/products/:product_id/edit" component={withAuth(ProductForm)} />
        <Route exact path="/products/:product_id" render={props =>
          <div>
            <ProductDetails
              username={currentUser.user.username}
              {...props}
              removeError={removeError}
              error={error}
              removeProduct={removeProduct}
            />
          </div>
        } />
        <Route exact path="/products" render={props =>
          <div>
            <Products
              // profileImageUrl={currentUser.user.profileImageUrl}
              username={currentUser.user.username}
              {...props}
              removeError={removeError}
              error={error}
            />
          </div>
        } />
        <Route path="/" render={props =>
          <Homepage hideNavbar={hideNavbar} showNavbar={showNavbar} currentUser={currentUser} {...props} error={"Page not found"} />
        } />
      </Switch>
    </Container>
  );
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    error: state.error
  };
}

export default withRouter(connect(mapStateToProps, { fetchProducts, removeProduct, authUser, removeError, logout })(Main));