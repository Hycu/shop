import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../store/actions/auth";
import { Menu, Icon, Popup, Segment } from 'semantic-ui-react'

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    }
  }
  logout = event => {
    event.preventDefault();
    this.props.logout();
  }

  render() {
    if (!this.props.hide) {
      return (
        <Segment raised color="teal">
          <Menu secondary>
            <Menu.Item floated="left">
              <Link to="/">
                <Icon name="home" color="teal" size="big" />
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`/products`}><Popup trigger={<Icon name="list ul" size="big" color="teal" />} content="Show available products" /></Link>
            </Menu.Item>
            {this.props.currentUser.isAuthenticated ? (
              <Menu.Menu position="right">
                <Menu.Item>
                  <Link to={`/products/new`}><Popup trigger={<Icon name="add" size="big" color="teal" />} content="Add new product" /></Link>
                </Menu.Item>
                <Menu.Item>
                  <button onClick={this.logout}><Popup trigger={<Icon name="sign out" size="big" color="teal" />} content="Logout" /></button>
                </Menu.Item>
              </Menu.Menu>
            ) : (
                <Menu.Menu position="right">
                  <Menu.Item>
                    <Link to="/signup"><Popup trigger={<Icon name="signup" size="big" color="teal" />} content="Sign up" /></Link>
                  </Menu.Item>
                  <Menu.Item>
                    <Link to="/signin"><Popup trigger={<Icon name="sign in" size="big" color="teal" />} content="Log in" /></Link>
                  </Menu.Item>
                </Menu.Menu>
              )}
          </Menu>
        </Segment>
      );
    } else {
      return (<div></div>);
    }
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps, { logout })(Navbar);