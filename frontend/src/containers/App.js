import React, { Component } from 'react';
import { Provider } from "react-redux";
import { configureStore } from "../store";
import { BrowserRouter as Router } from "react-router-dom";
import Navbar from "./Navbar";
import Main from "./Main";
import { setAuthorizationToken, setCurrentUser } from "../store/actions/auth";
import jwtDecode from "jwt-decode";

const store = configureStore();

if (localStorage.jwtToken) {
  setAuthorizationToken(localStorage.jwtToken);
  try {
    store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
  } catch (err) {
    store.dispatch(setCurrentUser({}));
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideNav: false
    }
    this.hideNavbar = this.hideNavbar.bind(this);
    this.showNavbar = this.showNavbar.bind(this);
  }

  hideNavbar() {
    this.setState({ hideNav: true });
  }

  showNavbar() {
    this.setState({ hideNav: false });
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Navbar hide={this.state.hideNav} Router />
            <Main hideNavbar={this.hideNavbar} showNavbar={this.showNavbar} Router />
          </div>
        </Router>
      </Provider>
    );
  }
};
export default App;
