import React, { Component } from "react";
import { connect } from "react-redux";
import { removeProduct, fetchProducts } from "../store/actions/product";
import ProductPreview from "../components/ProductPreview";
import { List } from 'semantic-ui-react'

class ProductList extends Component {

  render() {
    const { products, removeProduct, currentUser } = this.props;
    let productList = products.map(p => (
      <ProductPreview
        key={p._id}
        id={p._id}
        price={p.price}
        text={p.description.slice(0, 200) + "..."}
        name={p.name}
        image={p.image}
        category={p.category}
        removeProduct={this.props.handleRemove.bind(this, [p._id, p.category])}
        isCorrectUser={currentUser.user.id === p.author.id}
      />
    ));
    return (
      // <div className="row col-sm-8">
      // {/* <div className="offset-1 col-sm-10"> */}
        <List>
          {/* <ul className="list-group" id="messages"> */}
          {productList}
          {/* </ul> */}
        </List>
      // </div>
      // </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    products: state.products,
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps, { removeProduct, fetchProducts })(ProductList);