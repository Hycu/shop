import React, { Component } from "react";
import { connect } from "react-redux";
import { postNewProduct, fetchProducts, editProduct } from "../store/actions/product";
import { removeError } from "../store/actions/error";
import { Message, Grid, Label, Segment, Divider, Form, Input, Button, TextArea, Select, Loader } from 'semantic-ui-react';

class ProductForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      price: "",
      image: "",
      description: "",
      category: "",
      material: "",
      adjective: "",
      color: "",
      edit: false,
      loading: true
    };
    this.handleNewProduct = this.handleNewProduct.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async componentWillMount() {
    if (this.props.match.params.product_id) {
      await this.props.fetchProducts(`/${this.props.match.params.product_id}`);
      if (this.props.currentUser.isAuthenticated && this.props.currentUser.user.id === this.props.products[0].author.id) {
        await this.setState({ ...this.props.products[0], material: this.props.products[0].infos[0], adjective: this.props.products[0].infos[1], color: this.props.products[0].infos[2], edit: true, loading: false });
      } else {
        this.props.history.push("/signin");
      }
    } else {
      await this.setState({ loading: false });
    }
  }

  async handleNewProduct(event) {
    event.preventDefault();
    let error = false;
    let path = "/products";
    if (!this.state.edit) {
      error = await this.props.postNewProduct({
        name: this.state.name,
        price: this.state.price,
        image: this.state.image,
        description: this.state.description,
        category: this.state.category,
        infos: [
          this.state.material,
          this.state.adjective,
          this.state.color
        ]
      });
    } else {
      error = await this.props.editProduct({
        name: this.state.name,
        price: this.state.price,
        image: this.state.image,
        description: this.state.description,
        category: this.state.category,
        infos: [
          this.state.material,
          this.state.adjective,
          this.state.color
        ]
      }, this.props.match.params.product_id);
      path = path + `/${this.props.match.params.product_id}`;
    }
    if (!error) {
      await this.setState({
        name: "",
        price: "",
        image: "",
        description: "",
        category: "",
        material: "",
        adjective: "",
        color: ""
      });
      this.props.history.push(path);
    }

  };

  handleChange = (event, newState) => {
    this.setState({ [newState.name]: newState.value });
  }

  render() {
    const { history, removeError } = this.props;

    history.listen(() => {
      removeError();
    });
    let buttonText = "Add my product!";
    this.state.edit ? buttonText = "Edit my product!" : 0;
    if (this.state.loading) {
      return (
        <Loader active />
      );
    } else {
      return (
        <Grid centered columns="equal">
          <Divider hidden />
          <Grid.Row>
            {this.props.error.message && (
              <Message negative>
                {this.props.error.message}
              </Message>
            )}
          </Grid.Row>
          <Form onSubmit={this.handleNewProduct}>
            <Segment raised color="teal">
              <Form.Field>
                <Label pointing="below">Name (required)</Label>
                <Input
                  transparent
                  placeholder="Name..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.name}
                  name="name"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Price (required)</Label>
                <Input
                  transparent
                  onChange={this.handleChange}
                  type="number"
                  value={this.state.price}
                  name="price"
                  step="0.01"
                  placeholder="0.00"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Image URL</Label>
                <Input
                  transparent
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.image}
                  name="image"
                  placeholder="http://www.my_images.com/img.jpeg"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Material</Label>
                <Input
                  transparent
                  placeholder="Material..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.material}
                  name="material"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Adjective</Label>
                <Input
                  transparent
                  placeholder="Adjective..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.adjective}
                  name="adjective"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Color</Label>
                <Input
                  transparent
                  placeholder="Color..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.color}
                  name="color"
                />
              </Form.Field>
              <Divider />
              <Form.Field>
                <Label pointing="below">Category (required)</Label>
                <Select
                  placeholder="Choose category..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.category}
                  name="category"
                  options={[{
                    key: "Casual",
                    value: "Casual",
                    text: "Casual"
                  }, {
                    key: "Office",
                    value: "Office",
                    text: "Office"
                  }, {
                    key: "Outdoor",
                    value: "Outdoor",
                    text: "Outdoor"
                  }]}
                />
              </Form.Field>
              <Divider hidden />
              <Form.Field>
                <Label pointing="below">Description (required)</Label>
                <TextArea
                  transparent="true"
                  autoHeight
                  placeholder="Description..."
                  onChange={this.handleChange}
                  type="text"
                  value={this.state.description}
                  name="description"
                />
              </Form.Field>
              <Divider hidden />
              <Button type="submit" color="teal">
                {buttonText}
              </Button>
            </Segment>
          </Form>
        </Grid>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    error: state.error,
    products: state.products,
    currentUser: state.currentUser
  };
}

export default connect(mapStateToProps, { postNewProduct, removeError, fetchProducts, editProduct })(ProductForm);